// ==UserScript==
// @name        GooglePatents-Analysis
// @icon        https://rosauntier.com/wp-content/uploads/2019/05/cropped-logo-1.png
// @namespace   RosaUntier
// @match       https://patents.google.com/*&before=*:*1231&after=*:*0101
// @grant       none
// @version     0.2.1
// @author      RosaUntier
// @description Gets results from Google Patents
// @downloadURL https://gitlab.com/rosauntier/greasypatentanalysis/-/raw/master/greasypatentanalysis.user.js
// ==/UserScript==

const MINIMUM_YEAR = 1870
const currentUrl = window.location.href;
const regE = RegExp(/\d\d\d\d\d\d\d\d/gm);
const year = Number(currentUrl.match(regE)[0].slice(0, 4));
const isEnabled = year > MINIMUM_YEAR;
console.log(year, MINIMUM_YEAR, isEnabled)

// Check if minimum reached
if (!isEnabled) {

  alert('THE END')

  var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(localStorage));
  var downloadAnchorNode = document.createElement('a');
  downloadAnchorNode.setAttribute("href",     dataStr);
  downloadAnchorNode.setAttribute("download", "patents" + ".json");
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
  
  localStorage.clear();
  return
}


window.setTimeout(() => {
  var results = document.getElementsByClassName("style-scope search-results")[23].innerText;
  localStorage[year] = results
  
  

  const newUrl = currentUrl.replaceAll(regE, (result) => {
    const newYear = year - 1;
    const rest = result.slice(4);
    console.log(result, newYear, rest, newYear + rest);
    return newYear + rest
  
  });
  window.location.href = newUrl;
  }, 4000);

