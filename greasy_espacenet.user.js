// ==UserScript==
// @name        GreasyEspacenet
// @namespace   RosaUntier
// @icon        https://rosauntier.com/wp-content/uploads/2019/05/cropped-logo-1.png
// @match       https://worldwide.espacenet.com/patent/search*?q=*within%20%22*%22
// @grant       none
// @version     0.1
// @author      RosaUntier
// @description Gets results from EPO Espacenet
// @downloadURL https://gitlab.com/rosauntier/greasypatentanalysis/-/raw/master/greasy_espacenet.user.js
// ==/UserScript==



const WAIT_TIME = 5000
const MINIMUM_YEAR = 1960
const currentUrl = window.location.href;
const regE = RegExp(/within%20%22\d\d\d\d%22/gm);
const year = Number(currentUrl.match(regE)[0].slice(12, 16));
const isEnabled = year > MINIMUM_YEAR;
console.log(year, MINIMUM_YEAR, isEnabled)

// Check if minimum reached
if (!isEnabled) {

  alert('THE END')

  var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(localStorage));
  var downloadAnchorNode = document.createElement('a');
  downloadAnchorNode.setAttribute("href",     dataStr);
  downloadAnchorNode.setAttribute("download", "patents" + ".json");
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
  
  localStorage.clear();
  return
}


window.setTimeout(() => {
  var results = (document.getElementsByClassName("publications-list-header__tooltip-wrapper--37_PQZvC").length
                  && document.getElementsByClassName("publications-list-header__tooltip-wrapper--37_PQZvC")[0].innerText.replaceAll(" Treffer gefunden","")
                 ) || 1
  localStorage[year] = results
  
  

  const newUrl = currentUrl.replaceAll(regE, (result) => {
    const newYear = year - 1;
    return "within%20%22"+newYear+"%22"
  
  });
  window.location.href = newUrl;
  }, WAIT_TIME);

